# autofeedback-maven-archetype

[Maven](https://maven.apache.org/) archetype for building model solutions and starting code to use in [AutoFeedback](https://gitlab.com/autofeedback/autofeedback-webapp).

## How to use

Create an assessment in your AutoFeedback module, and write down its assessment ID.
You can find this out from the URL of the "Model solution" section of the assessment.
For instance, if the URL is `http://localhost:3000/modules/1/assessments/1/solution`, then the assessment ID is `1` (from `assessments/1/`).

From a terminal, navigate to the intended parent folder of your new model solution project.
If you are happy to use the default values for the main class name (`MainClass`) and the server URL (http://localhost:3000), and run this command:

```shell
mvn archetype:generate -Dfilter=autofeedback
```

If you would like to customise those options as well, run this command instead:

```shell
mvn archetype:generate -Dfilter=autofeedback -DaskForDefaultPropertyValues
```

When prompted for a selection, enter the number for this archetype (most likely `1`), and answer the various questions.
If there is a default value, you can press Enter to use it:

* `app-name`: unqualified name of the main class (e.g. `Main`). Any valid Java identifier should work (including those with Unicode characters).
* `af-server-url`: root URL of your AutoFeedback server (e.g. http://localhost:3000).
* `af-assessment-num`: enter the assessment ID that you wrote down before.
* `groupId`: Maven group ID for the new project (e.g. `uk.ac.youruni.yourmodule`).
* `artifactId`: Maven artifact ID for the new project (e.g. `intro-lab`).
* `version`: you can usually just press Enter to accept the default value.
* `package`: fully-qualified name of the root Java package. If your `groupId` follows the style suggested above, you can just press Enter.

## Support

Please email `a DOT garcia DASH dominguez AT york DOT ac DOT uk` if you have questions.

## How to build

Install Maven (see link above) and run this command from the root of the repository:

```shell
mvn clean install
```

## Contributing

Merge requests are welcome. Please ensure that any changes include both documentation and tests.

To release a new stable version to Central, ask one of the maintainers to create a tag of the form `X.Y.Z` (a version number according to [semantic versioning rules](https://semver.org)) from the `main` branch.

## Authors and acknowledgment

* Tony Beaumont: initial version
* Antonio Garcia-Dominguez: refinements and packaging

## License

This project is under the Apache License 2.0 (see [`LICENSE-2.0.txt`](LICENSE-2.0.txt)).
