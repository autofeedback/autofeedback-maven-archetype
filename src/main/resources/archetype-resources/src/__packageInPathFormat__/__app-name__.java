#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};


public class ${app-name} {

	public void doSomething() {
		// TODO - write your code here
		System.out.println("Sample method provided by the archetype!");
	}

	public static void main(String[] args) {
		${app-name} myObject = new ${app-name}();
		myObject.doSomething();
	}
}
